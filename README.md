# Passion - Cross-Server
Passion running off a database, meant to be used for multi-server access.

## What is Passion?
Passion is an all-in-one Discord administration bot created by Chill using the [JDA Discord Library](https://github.com/DV8FromTheWorld/JDA). It was made to practice and explore the endless amount of features Java provides.

Currently, Passion has the core administration features completed such as muting, auto-role assignment and spam detection.

## Setting up Passion for your own use:

### Pre-requisites:
Installing and getting Passion to work is relatively simple, the pre-requisites you might need will be:
1. Some experience in Java, preferably understanding of core OOP concepts.
2. Have a database instance ready to be used to host the data that will be accessed and stored.

### Installation
1. Clone this repository into your local machine and open up the project in your IDE/text editor of choice, 
2. Create a file `Ref.java` and inside of it, add your bot token as a static constant. (Refer to example 1.0)
3. Within the same file, add additional constants for your database url, username and password. (Refer to example 1.1) For my instance, I decided to use PostgreSQL to create the database for Passion, however, you can feel free to install and use other types of RDBMS languages to contain the database. (Refer to `Supporting other RDBMS software`)
4. Locate the SQL file included in the `res` folder and execute the statements to create the necessary tables.
5. You have set up your own instance of Passion to be used for your server(s). Refer to `Getting started with Passion` to begin using Passion and editing it.

## Supporting other RDBMS software:
My instance of passion uses PostgreSQL to run and store the data, however, you can feel free to use other RDBMS software, just ensure your SQL queries are updated and you are using the appropriate drivers

### Updating SQL queries:
**MySQL Queries**

The queries using MySQL does not differ to that of PostgreSQL, there is not need to worry about compatibility.
```sql
CREATE TABLE PassionGuild (
  guild_id TEXT NOT NULL,
  guild_command TEXT NULL,
  guild_general TEXT NULL,
  guild_suggestion TEXT NULL,
  guild_warning TEXT NULL,

  PRIMARY KEY (guild_id)
);

CREATE TABLE Macro (
  macro_name TEXT NOT NULL,
  guild_id TEXT NOT NULL,
  macro_text TEXT NOT NULL,

  PRIMARY KEY (macro_name, guild_id)
);

CREATE TABLE Prefix (
  guild_id TEXT NOT NULL,
  prefix TEXT NOT NULL,

  PRIMARY KEY (guild_id)
);
```  

### Including other RDBMS drivers
To include other RDBMS drivers, you will need to perform 2 edits.

**pom.xml**

In `pom.xml`, add a maven repository for the respective driver you are intending to install, for instance, this is the driver for [MySQL](https://mvnrepository.com/artifact/mysql/mysql-connector-java)

**DbConnection.java**

In `DbConnection.java`, find and edit the line within the `DbConnection` constructor, changing the contents of the string to that of the intended driver.
```java
public class DbConnection {
	// ...
	private DbConnection () {
		// ...
		Class.forName("org.postgresql.Driver");
		// ....
	}
	// ...
}
```

### Getting started with Passion
#### Basics:
The default prefix for Passion is the `?` symbol, this is editable using the `?setprefix <prefix>` command provided. The remainder of this guide will be assuming that the original prefix has not been modified.

**Roles**

When you first add Passion to your server, you (as the owner) will need to use the `?roles` command to automatically generate and assign the basic roles, ie `Member` to all existing members, `Owner` to the owner and `Slave` to any existing bots.

The automatically generated roles include:
```
Owner
Administrator
Sr Moderator
Moderator
Member
Slave
Muted
```

This structure plays an important role as they are how you are able to allow a select number of users to execute commands whilst preventing others from using restricted commands.

**Logging**

After using the `?roles` command, you will need to begin setting the automatic logging channels, namely, the core 2 channels that have to be set are the command logging channel and the general chat channel.

The command logging channel will be used to record down command invocations from around the server, you can do this by using the `?setlog` in the desired channel and Passion will begin logging into this channel.

The general chat channel will be what is used to display general information such as the bot's welcome messages. Use the `?setgeneral` command in the desired channel and Passion will begin greeting newcomers in this channel!

The other logging channels will be:
1. Suggestion channel that work hand-in-hand with the the `?suggest` command.
2. Warning channel that will log any warnings and potential errors, such as when a raider is caught

**Prefix**

If you prefer to use another prefix, you can set it via the `?setprefix <target>` command and if you do not like the changes, use the `?resetprefix` command to set it back to `?`

#### Advanced
**Optional Arguments**

In order to allow for flexibility in the program, I have included a system where you can use optional arguments with certain commands. For example, the `mute` command takes 2 arguments, the first being the `mute duration` in minutes and the second is the `target user id`. The first argument can be replaced with a `*` symbol to specify using the default value specified (15 minutes), so using the command like `?mute * 302385772718325760` will mute the target for 15 minutes.

**Adding custom commands**

If you wish to include additional commands of your own to Passion, you can do so by editing the source code of Passion.

Steps to add a new command:
1. If your command is within a category of it's own, create a corresponding class following this naming convention `<Category>Commands.java`
2. If you created a new class, you will need to add it to the `CommandContainer` class. (Refer to Example 1.2)
3. In the newly-created or existing class, look for the constructor and start creating the custom command.
4. Simply call the `.add(Command c)` static method on `CommandContainer` and proceed to create an anonymous inner class instance of the `Command` class. (Refer to Example 1.3)
5. When you first create the `Command` instance, you need to specify a name for the command, bear in the mind the command name should not conflict with existing commands or macros.
6. Following the command name, you can decide to specify a minimum role required by the invoker, these use the automatically created roles mentioned previously, select the appropriate one for the command. If you do not specify anything, it will default to being available to all users.
7. Next, if the custom command requires arguments, specify that at the end of the constructor parameter list. Use the `new Argumet.ArgumentBuilder()` builder to start creating arguments.
8. Next, override the `.invoke` method and within it, specify what the command will perform. You can access the arguments given by the invoker from the `args` String array.
9. Refresh Passion to see if the command works.

**Adding optional arguments for commands**

You can easily include an optional argument in your command using the `ArgumentBuilder`.

1. On the argument that you wish to set to optional, chain the `.setToOptional(String str)` method on it, specifying the default value in the event where the user does decide to use the default value (Refer to Example 1.4)
2. Test out the changes 

### Others:
Passion is free to use by anyone, feel free to download it and toy with it. It is still in its developmental stages.

If you wish contribute to this project, feel free to do so.

If you do use Passion in your own server, give me a shoutout, drop me a DM or contact me on GitHub so I can check out how it is working out for you!

You can drop by the Passion development server: https://discord.gg/ceTPhqR

### Code Examples:
#### Example 1.0
This is the instance of the `Ref.java` class you should include in your project
```java
public class Ref {
	public static final String TOKEN = "<bot_token>";
}
```

#### Example 1.1
Add additional constants to your class for your database to connect
```java
public class Ref {
	// ...
	
	// Database stuff
	public static final String URL = "<database_url>";
	public static final String USER = "<username>";
	public static final String PASS = "<password>";
}
```

#### Example 1.2
If you want to made a new category, create a new class like this:
```java
public class CategoryCommands {
	// ...
}
```
And add it to the `CommandContainer` class:
```java
public class CommandContainer {
	// ...
	public static void load() {
		// ...
		new CategoryCommand();
	}
}
```

#### Example 1.3
For this example, I will be showing how to create a simple echo command, which will have the following format: `?echo <channel_id> <message>`.

To add a new command, go into the respective category and add the following:
```java
public class CategoryCommands {
	// I want the name to be "echo", only accessible to Moderator+ and take in 2 arguments
	public CategoryCommands () {
		CommandContainer.add(
		    new Command("echo", PassionRole.MODERATOR, 
		        new Argument.ArgumentBuilder().setType(ArgumentType.CHANNEL_ID).build(),
		        new Argument.ArgumentBuilder().setType(ArgumentType.SENTENCE).build()) {
		    	
                    @Override
                    public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
                        String targetChannelId = args[0];
                        String message = args[1];
                    
                        MessageChannel targetChannel = guild.getTextChannelById(targetChannelId);
                        targetChannel.sendMessage(message).queue(); 
                    }
		    }	
		);
	}
}
```
With that, we have created a simple echo command that can be used. This is an existing Passion command.

#### Example 1.4
In this example, we will be creating a command that can be used to roll a die. You can specify the number of sides of a die or you can use the default value of 6. The command structure will look like `?roll <sides>`.

```java
public class FunCommands {
	public FunCommands () {
		CommandContainer.add(
		    new Command("roll", PassionRole.MEMBER, 
		    new Argument.ArgumentBuilder().setType(ArgumentType.INT).setToOptional("6").build()) {
		    	
		    	@Override
		    	public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
		    		int sides = Integer.parseInt(args[0]);
		    		int roll = new Random().nextInt(sides) + 1;
		    		
		    		from.sendMessage("You rolled a " + roll).queue();
		    	}
		    }	
		);
	}
}
```
This means that if the user invokes the command like this: `?roll *`, it will roll from 1 - 6, but if they invoke it like this `?roll 12`, it will roll from 1 - 12.