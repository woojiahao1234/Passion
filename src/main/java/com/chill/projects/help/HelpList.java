package com.chill.projects.help;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HelpList {
	private static List<Help> helpList = load();
	private static List<String> categories;
	private static final String FILE_PATH = "res/commands.json";

	public static List<String> getCategories () {
		return categories;
	}

	public static Help findHelp (String name, Member member, Guild from) {
		for (Help h : helpList) {
			if (h.getName().equalsIgnoreCase(name)) {
				if (hasPermission(name, member, from)) {
					return h;
				}
			}
		}
		return null;
	}

	public static List<String> findCategory (String category, Member member, Guild from) {
		List<String> result = new ArrayList<>();
		for (Help h : helpList) {
			if (h.getCategory().equalsIgnoreCase(category)) {
				if (hasPermission(h.getName(), member, from)) {
					result.add(h.getName());
				}
			}
		}

		return result;
	}

	private static boolean hasPermission (String toFind, Member invoker, Guild from) {
		Command attempt = CommandContainer.find(toFind);
		if (attempt == null) return false;

		Role minimum = from.getRolesByName(attempt.getMinimum().getName(), true).get(0);
		Role invokerRole = invoker.getRoles().get(0);

		int minimumPos = minimum.getPosition();
		int invokerPos = invokerRole.getPosition();

		return invokerPos >= minimumPos;
	}

	private static List<Help> load () {
		categories = new ArrayList<>();

		List<Help> result = new ArrayList<>();

		String jsonFile = loadJSON();

		JSONParser parser = new JSONParser();
		JSONObject parent = null;
		try {
			parent = (JSONObject) parser.parse(jsonFile);
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("Unable to parse commands.json");
		}

		if (parent == null) return null;

		JSONObject commands = (JSONObject) parent.get("commands");

		for (Object category : commands.entrySet()) {
			Map.Entry entry = (Map.Entry) category;
			Help.Builder builder = new Help.Builder();
			builder.setCategory((String) entry.getKey());

			if (!categories.contains(entry.getKey())) {
				categories.add((String) entry.getKey());
			}

			JSONArray internalCommands = (JSONArray) entry.getValue();

			Iterator iterator = internalCommands.iterator();
			while (iterator.hasNext()) {
				JSONObject cur = (JSONObject) iterator.next();

				String name = (String) cur.get("name");
				String syntax = (String) cur.get("syntax");
				String description = (String) cur.get("description");
				String example = (String) cur.get("example");
				String optional = (String) cur.get("optional");

				Help toAdd = builder.setName(name)
					.setExample(example)
					.setDescription(description)
					.setOptional(optional)
					.setSyntax(syntax)
					.build();

				result.add(toAdd);
			}
		}


		return result;
	}

	private static String loadJSON () {
		StringBuilder result = new StringBuilder();

		try (BufferedReader in = new BufferedReader(new FileReader(FILE_PATH))) {
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Unable to load commands.json file");
		}

		return result.toString();
	}
}
