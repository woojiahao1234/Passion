package com.chill.projects.help;

public class Help {
	private String category;
	private String name;
	private String syntax;
	private String description;
	private String example;
	private String optional;

	public String getCategory () {
		return category;
	}

	public String getName () {
		return name;
	}

	public String getSyntax () {
		return syntax;
	}

	public String getDescription () {
		return description;
	}

	public String getExample () {
		return example;
	}

	public String getOptional () {
		return optional;
	}

	public static class Builder {
		private String category;
		private String name;
		private String syntax;
		private String description;
		private String example;
		private String optional;


		public Builder setCategory (String category) {
			this.category = category;
			return this;
		}

		public Builder setName (String name) {
			this.name = name;
			return this;
		}

		public Builder setSyntax (String syntax) {
			this.syntax = syntax;
			return this;
		}

		public Builder setDescription (String description) {
			this.description = description;
			return this;
		}

		public Builder setExample (String example) {
			this.example = example;
			return this;
		}

		public Builder setOptional (String optional) {
			this.optional = optional;
			return this;
		}

		public Help build () {
			return new Help(category, name, syntax, description, example, optional);
		}
	}

	private Help (String category, String name, String syntax,
				  String description, String example, String optional) {

		this.category = category;
		this.name = name;
		this.syntax = syntax;
		this.description = description;
		this.example = example;
		this.optional = optional;
	}
}
