package com.chill.projects.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IQueryData {
	ResultSet select (String cols, String table, String where) throws SQLException;
	void update (String set, String table, String where) throws SQLException;
	void delete (String table, String where) throws SQLException;
	void insert (String cols, String table, String vals) throws SQLException;
}
