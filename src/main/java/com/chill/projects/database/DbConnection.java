package com.chill.projects.database;

import com.chill.projects.Ref;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

	private static DbConnection instance = new DbConnection();
	private Connection connection;

	private DbConnection () {
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(Ref.URL, Ref.USER, Ref.PASS);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection () {
		return connection;
	}

	public synchronized static DbConnection getInstance () throws SQLException {
		if (instance == null) {
			instance = new DbConnection();
		}
		return instance;
	}
}
