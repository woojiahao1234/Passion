package com.chill.projects.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QueryData implements IQueryData {

	@Override
	public ResultSet select (String cols, String table, String where) throws SQLException {

		String query = "SELECT " + cols + " FROM " + table;
		if (!where.equals("")) {
			query += " WHERE " + where;
		}

		Statement statement = createStatement();
		return statement.executeQuery(query);
	}

	@Override
	public void update (String set, String table, String where) throws SQLException {

		String query = "UPDATE " + table + " SET " + set;
		if (!where.equals("")) {
			query += " WHERE " + where;
		}

		Statement statement = createStatement();
		statement.executeUpdate(query);
	}

	@Override
	public void delete (String table, String where) throws SQLException {

		String query = "DELETE FROM " + table;
		if (!where.equals("")) {
			query += " WHERE " + where;
		}

		Statement statement = createStatement();
		statement.executeUpdate(query);
	}

	@Override
	public void insert (String cols, String table, String vals) throws SQLException {

		String query = "INSERT INTO " + table + " ( " + cols + " ) VALUES ( " + vals + " )";

		Statement statement = createStatement();
		statement.executeUpdate(query);
	}

	private PreparedStatement prepareStatement (String query) throws SQLException {
		return DbConnection.getInstance().getConnection().prepareStatement(query);
	}

	private Statement createStatement () throws SQLException {
		return DbConnection.getInstance().getConnection().createStatement();
	}
}
