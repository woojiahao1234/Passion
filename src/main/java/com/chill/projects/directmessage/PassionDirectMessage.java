package com.chill.projects.directmessage;

import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;

public class PassionDirectMessage {

	public void sendDirectMessage (String message, User target) {
		target.openPrivateChannel().queue(channel -> channel.sendMessage(message).queue());
	}

	public void sendDirectMessage (MessageEmbed embed, User target) {
		target.openPrivateChannel().queue(channel -> channel.sendMessage(embed).queue());
	}
}
