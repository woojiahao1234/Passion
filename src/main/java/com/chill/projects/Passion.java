package com.chill.projects;

import com.chill.projects.commands.listeners.*;
import com.chill.projects.commands.listeners.spam.SpamListener;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.logging.Logging;
import com.chill.projects.macros.MacrosList;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;

import javax.security.auth.login.LoginException;
import java.sql.SQLException;

public class Passion {

	private static JDA jda;

	public static void main (String[] args) {

		Logging logger = new Logging();

		try {
			jda = new JDABuilder(AccountType.BOT)
				.setToken(Ref.TOKEN)
				.setStatus(OnlineStatus.ONLINE)
				.buildAsync();
			jda.getPresence().setGame(Game.of(Game.GameType.DEFAULT, "Being useful, unlike Chill"));

			PassionGuildList.load(jda.getGuilds());
			MacrosList.load();

			jda.addEventListener(
				new OnJoinListener(),
				new OnLeaveListener(logger),
				new CommandListener(logger),
				new MacroListener(logger),
				new SpamListener(logger),
				new InviteListener(logger)
			);
		} catch (LoginException | SQLException e) {
			e.printStackTrace();
		}
	}
}
