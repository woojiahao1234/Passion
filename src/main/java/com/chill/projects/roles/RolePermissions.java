package com.chill.projects.roles;

import net.dv8tion.jda.core.Permission;
import org.apache.commons.lang.ArrayUtils;

public class RolePermissions {
	public static Permission[] memberPermissions = {
		Permission.VIEW_AUDIT_LOGS, Permission.NICKNAME_CHANGE,
		Permission.MESSAGE_READ, Permission.MESSAGE_HISTORY,
		Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES,
		Permission.MESSAGE_EXT_EMOJI, Permission.MESSAGE_ADD_REACTION,
		Permission.VOICE_CONNECT, Permission.VOICE_SPEAK
	};

	private static Permission[] modPerms = {
		Permission.BAN_MEMBERS, Permission.VIEW_AUDIT_LOGS,
		Permission.CREATE_INSTANT_INVITE, Permission.NICKNAME_MANAGE,
		Permission.MESSAGE_MANAGE
	};

	public static Permission[] moderatorPermissions = (Permission[]) ArrayUtils.addAll(memberPermissions, modPerms);
}
