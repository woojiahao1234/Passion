package com.chill.projects.roles;

import net.dv8tion.jda.core.Permission;

import java.awt.*;

public enum PassionRole {
	OWNER ("Owner", new Color(30, 255, 236), Permission.ADMINISTRATOR),
	ADMINISTRATOR ("Administrator", new Color(255, 86, 30), Permission.ADMINISTRATOR),
	SLAVE ("Slave", new Color(219, 122, 255), Permission.ADMINISTRATOR),
	MUTED ("Muted", new Color(255, 53, 53), Permission.UNKNOWN),
	SRMODERATOR ("Sr Moderator", new Color(71, 247, 32), Permission.ADMINISTRATOR),
	MODERATOR ("Moderator", new Color(31, 168, 247), RolePermissions.moderatorPermissions),
	MEMBER ("Member", new Color(255, 226, 66), RolePermissions.memberPermissions);

	private String name;
	private Color color;
	private Permission[] permissions;

	PassionRole (String name, Color color, Permission... permissions) {
		this.name = name;
		this.color = color;
		this.permissions = permissions;
	}

	public String getName () {
		return name;
	}

	public Color getColor () {
		return color;
	}

	public Permission[] getPermissions () {
		return permissions;
	}

}
