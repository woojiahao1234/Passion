package com.chill.projects.roles;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;

public class RoleManager {

	public void loadRoles (Guild guild) {
		for (PassionRole p : PassionRole.values()) {
			if (!doesRoleExist(guild, p.getName())) {
				createRole(guild, p);
			}
		}
	}

	public void autoAssignRoles (Guild guild) {
		guild.getMembers().forEach(member -> {
			giveRole(member, guild, PassionRole.MEMBER);

			if (member.getUser().isBot()) {
				giveRole(member, guild, PassionRole.SLAVE);
			}
			if (member.isOwner()) {
				giveRole(member, guild, PassionRole.OWNER);
			}
		});
	}

	public void giveRole (Member target, Guild guild, PassionRole role) {
		guild
			.getController()
			.addSingleRoleToMember(
				target,
				guild.getRolesByName(role.getName(), true)
					.get(0))
			.complete();
	}

	public void removeRole (Member target, Guild guild, PassionRole role) {
		guild
			.getController()
			.removeSingleRoleFromMember(
				target,
				guild.getRolesByName(role.getName(), true).get(0))
			.complete();
	}

	private boolean doesRoleExist (Guild guild, String roleName) {
		return guild.getRolesByName(roleName, true).size() != 0;
	}

	private void createRole (Guild guild, PassionRole role) {
		guild.getController()
			.createRole()
			.setName(role.getName())
			.setColor(role.getColor())
			.setPermissions(role.getPermissions())
			.complete();
	}
}
