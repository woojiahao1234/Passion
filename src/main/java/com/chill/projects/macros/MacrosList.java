package com.chill.projects.macros;

import com.chill.projects.database.IQueryData;
import com.chill.projects.database.QueryData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MacrosList {

	private static List<Macro> macros = new ArrayList<>();
	private static IQueryData queryData = new QueryData();

	public static void load () {
		try {
			ResultSet resultSet = queryData.select("*", "Macro", "");

			while (resultSet.next()) {
				String name = resultSet.getString(1);
				String guildId = resultSet.getString(2);
				String text = resultSet.getString(3);

				Macro toAdd = new Macro(name, guildId, text);
				macros.add(toAdd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Macro> getMacros () {
		return macros;
	}

	public static Macro find (String name, String guildId) {
		for (Macro m : macros) {
			if (m.getName().equals(name) && m.getGuildId().equals(guildId)) {
				return m;
			}
		}

		return null;
	}

	public static void add (Macro macro) {
		macros.add(macro);

		String values = String.format("'%s', '%s', '%s'", macro.getName(), macro.getGuildId(), macro.getText());
		try {
			queryData.insert("macro_name, guild_id, macro_text", "Macro", values);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void remove (String name, String guildId) {
		macros.remove(find(name, guildId));

		String where = String.format("macro_name = '%s' AND guild_id = '%s'", name, guildId);
		try {
			queryData.delete("Macro", where);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
