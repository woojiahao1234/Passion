package com.chill.projects.macros;

public class Macro {

	private String name;
	private String guildId;
	private String text;

	public Macro (String name, String guildId, String text) {
		this.name = name;
		this.guildId = guildId;
		this.text = text;
	}

	public String getName () {
		return name;
	}

	public String getGuildId () {
		return guildId;
	}

	public String getText () {
		return text;
	}
}
