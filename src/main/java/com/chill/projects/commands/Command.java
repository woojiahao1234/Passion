package com.chill.projects.commands;

import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentChecking;
import com.chill.projects.roles.PassionRole;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import static com.chill.projects.roles.PassionRole.*;

public class Command implements ICommand {

	private String name;
	private PassionRole minimum;
	private Argument[] arguments;

	protected Command (String name) {
		this(name, MEMBER);
	}

	protected Command (String name, PassionRole minimum) {
		this(name, minimum, new Argument[] { });
	}

	protected Command (String name, Argument... expectedArgs) {
		this(name, MEMBER, expectedArgs);
	}

	protected Command (String name, PassionRole minimum, Argument... expectedArgs) {
		this.name = name;
		this.minimum = minimum;
		this.arguments = expectedArgs;
	}

	public String getName () {
		return name;
	}

	public PassionRole getMinimum () {
		return minimum;
	}

	public Argument[] getArguments () {
		return arguments;
	}

	@Override
	public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
		from.sendMessage("Command invoked").queue();
	}

	public boolean hasValidArguments (String[] args, Guild guild, MessageChannel from) {
		for (int i = 0; i < arguments.length; i++) {
			if (!ArgumentChecking.getChecks().get(arguments[i].getType()).check(args[i], guild, from, arguments[i])) {
				return false;
			}
		}

		return true;
	}
}
