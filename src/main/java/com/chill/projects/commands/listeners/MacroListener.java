package com.chill.projects.commands.listeners;

import com.chill.projects.commands.CommandContainer;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.logging.Logging;
import com.chill.projects.macros.Macro;
import com.chill.projects.macros.MacrosList;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MacroListener extends ListenerAdapter {

	private Logging logger;

	public MacroListener (Logging logger) {
		this.logger = logger;
	}

	@Override
	public void onGuildMessageReceived (GuildMessageReceivedEvent event) {

		if (event.getAuthor().isBot()) return;

		String message = event.getMessage().getContentRaw();
		if (!message.startsWith(PassionGuildList.find(event.getGuild().getId()).getPrefix())) {
			return;
		}

		String macroUsed = retrieveCommandUsed(message);

		if (CommandContainer.find(macroUsed) != null) {
			return;
		}

		Macro macro;
		if ((macro = MacrosList.find(macroUsed, event.getGuild().getId())) == null) {
			event.getChannel().sendMessage("Unable to find macro: **" + macroUsed + "** on this server.").queue();
			return;
		}

		logUse(macroUsed, event.getGuild(), event.getAuthor(), event.getChannel());
		event.getChannel().sendMessage(macro.getText()).queue();
	}

	private String retrieveCommandUsed (String message) {
		message += " ";
		return message.substring(1, message.indexOf(" "));
	}

	private void logUse (String macroName, Guild guild, User invoker, MessageChannel from) {
		logger.normal(guild, "Macro: **" + macroName
			+ "** invoked by **" + invoker.getName() +
			"#" + invoker.getDiscriminator() + "** :: ID :: **" + invoker.getId()
			+ "** in **" + from.getName() + "**");
	}
}
