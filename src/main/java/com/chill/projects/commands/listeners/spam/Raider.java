package com.chill.projects.commands.listeners.spam;

public class Raider {

	private String userId;
	private String guildId;

	public Raider (String userId, String guildId) {
		this.guildId = guildId;
		this.userId = userId;
	}

	public String getGuildId () {
		return guildId;
	}

	public String getUserId () {
		return userId;
	}
}
