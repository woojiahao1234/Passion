package com.chill.projects.commands.listeners;

import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.logging.Logging;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.SQLException;

public class OnLeaveListener extends ListenerAdapter {

	private Logging logger;

	public OnLeaveListener (Logging logger) {
		this.logger = logger;
	}

	@Override
	public void onGuildLeave (GuildLeaveEvent event) {
		try {
			PassionGuildList.remove(event.getGuild().getId());
		} catch (SQLException e) {
			e.printStackTrace();
			event.getGuild().getDefaultChannel().sendMessage("OnLeave error").queue();
		}
	}

	@Override
	public void onGuildMemberLeave (GuildMemberLeaveEvent event) {
		logger.normal(event.getGuild(), "**" + event.getMember().getEffectiveName() + "** has left the server");
	}
}
