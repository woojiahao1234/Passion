package com.chill.projects.commands.listeners;

import com.chill.projects.guilds.PassionGuild;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.roles.PassionRole;
import com.chill.projects.roles.RoleManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.requests.RestAction;

import java.awt.*;
import java.sql.SQLException;

public class OnJoinListener extends ListenerAdapter {

	@Override
	public void onGuildJoin (GuildJoinEvent event) {
		Guild joined = event.getGuild();

		if (joined.getDefaultChannel() == null) {
			return;
		}

		String defaultChannelId = joined.getDefaultChannel().getId();
		PassionGuild toAdd = new PassionGuild(joined.getId(),
			defaultChannelId, defaultChannelId, defaultChannelId, defaultChannelId, "?");

		try {
			PassionGuildList.add(toAdd);
		} catch (SQLException e) {
			e.printStackTrace();
			joined.getDefaultChannel().sendMessage("Unable to add guild").queue();
		}
	}

	@Override
	public void onGuildMemberJoin (GuildMemberJoinEvent event) {
		User joined = event.getUser();
		Guild current = event.getGuild();

		String generalId = PassionGuildList.find(current.getId()).getGeneralId();

		EmbedBuilder welcomeEmbed = new EmbedBuilder();
		welcomeEmbed.addField("New class unlocked", "Player: " + joined.getAsMention(), false);
		welcomeEmbed.addField("Tutorial", "Read our rules-and-info, look around in the channel list, and say hi!", false);

		welcomeEmbed.setColor(new Color(66, 255, 122));
		welcomeEmbed.setThumbnail(joined.getAvatarUrl());

		RestAction<Message> ra = event.getGuild().getTextChannelById(generalId).sendMessage(welcomeEmbed.build());
		Message message = ra.complete();
		message.addReaction("\uD83D\uDC4B").complete();

		RoleManager manager = new RoleManager();
		manager.giveRole(event.getMember(), event.getGuild(), PassionRole.MEMBER);

		if (event.getUser().isBot()) {
			manager.giveRole(event.getMember(), event.getGuild(), PassionRole.SLAVE);
		}
	}
}
