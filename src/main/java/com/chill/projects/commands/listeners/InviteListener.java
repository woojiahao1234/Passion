package com.chill.projects.commands.listeners;

import com.chill.projects.logging.Logging;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class InviteListener extends ListenerAdapter {

	private Logging logger;

	public InviteListener (Logging logger) {
		this.logger = logger;
	}

	@Override
	public void onGuildMessageReceived (GuildMessageReceivedEvent event) {
		User sender = event.getAuthor();
		if (sender.isBot() || event.getGuild().getMember(sender).isOwner()) return;

		String message = event.getMessage().getContentRaw();
		if (!containsInvite(message)) return;
		event.getMessage().delete().complete();

		logWarning(event.getGuild(), sender, event.getChannel(), message);
	}

	private boolean containsInvite (String message) {
		return message.contains("https://discord.gg/");
	}

	private void logWarning (Guild guild, User sender, MessageChannel from, String message) {
		logger.warning(guild, sender.getName() + " sent: " + message + " in #" + from.getName());
	}
}
