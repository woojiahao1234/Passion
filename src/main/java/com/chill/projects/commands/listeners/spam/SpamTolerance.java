package com.chill.projects.commands.listeners.spam;

public enum SpamTolerance {
	MESSAGE (5, 3);

	private int messageCount;
	private int duration;

	SpamTolerance (int messageCount, int duration) {
		this.messageCount = messageCount;
		this.duration = duration;
	}

	public int getDuration () {
		return duration * 1000;
	}

	public int getMessageCount () {
		return messageCount;
	}
}
