package com.chill.projects.commands.listeners.spam;

import com.chill.projects.database.QueryData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RaidersList {

	private static List<Raider> raiders = load();

	public static boolean hasRaiderInGuild (String userId, String guildId) {
		return raiders
			.stream()
			.anyMatch(r -> r.getGuildId().equals(guildId)
				&& r.getUserId().equals(userId));
	}

	public static void add (String userId, String guildId) {
		raiders.add(new Raider(userId, guildId));

		String values = String.format("'%s', '%s'", userId, guildId);
		try {
			new QueryData().insert("user_id, guild_id", "Raider", values);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Unable to add raider");
		}
	}

	public static void remove (String userId, String guildId) throws SQLException {
		Raider toRemove = find(userId, guildId);
		raiders.remove(toRemove);

		String deleteWhere = String.format("user_id = '%s' AND guild_id = '%s'",
			userId, guildId);
		new QueryData().delete("Raider", deleteWhere);
	}

	public static Raider find (String userId, String guildId) {
		for (Raider r : raiders) {
			boolean isMatch = r.getUserId().equals(userId) && r.getGuildId().equals(guildId);

			if (isMatch) return r;
		}

		return null;
	}

	public static List<Raider> find (String guildId) {
		return raiders.stream()
			.filter(raider -> raider.getGuildId().equals(guildId))
			.collect(Collectors.toList());
	}

	private static List<Raider> load () {
		List<Raider> raiders = new ArrayList<>();

		try {

			ResultSet results = new QueryData().select("*", "Raider", "");

			while (results.next()) {
				String userId = results.getString(1);
				String guildId = results.getString(2);

				Raider toAdd = new Raider(userId, guildId);
				raiders.add(toAdd);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Unable to load raiders list");
		}

		return raiders;
	}

}
