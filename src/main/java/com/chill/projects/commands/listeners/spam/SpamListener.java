package com.chill.projects.commands.listeners.spam;

import com.chill.projects.directmessage.PassionDirectMessage;
import com.chill.projects.logging.Logging;
import com.chill.projects.roles.PassionRole;
import com.chill.projects.roles.RoleManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

public class SpamListener extends ListenerAdapter {

	private Logging logger;
	private RoleManager manager;

	private List<PotentialRaider> potentialRaiders;

	public SpamListener (Logging logger) {
		this.logger = logger;
		potentialRaiders = new ArrayList<>();
		manager = new RoleManager();
	}

	@Override
	public void onGuildMessageReceived (GuildMessageReceivedEvent event) {
		User sender = event.getAuthor();
		Guild current = event.getGuild();

			if (toIgnore(sender, current)) return;

		spamDetection(sender, current, event.getChannel());
	}

	private void spamDetection (User user, Guild guild, MessageChannel from) {
		String userId = user.getId();
		String guildId = guild.getId();

		if (!underWatch(userId, guildId)) {
			startWatching(userId, guildId);
			return;
		}

		PotentialRaider raider = getPotentialRaider(userId, guildId);

		if (raider == null) {
			return;
		}

		raider.incMessageCount();
		if (raider.getMessageCount() > SpamTolerance.MESSAGE.getMessageCount()) {
			long currentTime = System.currentTimeMillis();
			boolean isRaider = currentTime - raider.getInitialMessageTime() <= SpamTolerance.MESSAGE.getDuration();

			if (isRaider) {
				manager.giveRole(guild.getMember(user), guild, PassionRole.MUTED);

				logWarning(user, guild);
				sendWarningDM(user);

				removeRecentMessages(user, guild, from);

				RaidersList.add(userId, guildId);
			}
		}
	}

	private void removeRecentMessages (User user, Guild guild, MessageChannel from) {
		List<Message> toDelete = new MessageHistory(from)
			.retrievePast(50)
			.complete()
			.stream()
			.filter(sender -> sender.getAuthor().getId().equals(user.getId()))
			.collect(Collectors.toList());

		if (toDelete.size() <= 0) {
			return;
		}

		guild.getTextChannelById(from.getId()).deleteMessages(toDelete).complete();
	}

	private boolean toIgnore (User sender, Guild current) {
		boolean isOwner = current.getMember(sender).isOwner();
		boolean isBot = sender.isBot();
		boolean alreadyCaught = RaidersList.hasRaiderInGuild(sender.getId(), current.getId());

		return isBot || isOwner || alreadyCaught;
	}

	private void sendWarningDM (User user) {
		EmbedBuilder warningEmbed = new EmbedBuilder()
			.addField("Muted",
				"You have been muted due to spam, if you think you were " +
					"muted by accident, approach a moderator for an unmute",
				false)
			.setColor(new Color(255, 53, 53));
		new PassionDirectMessage().sendDirectMessage(warningEmbed.build(), user);
	}

	private void logWarning (User user, Guild guild) {
		String userId = user.getId();
		String warningMessage = String.format("**%s** :: ID :: **%s** has been muted under suspicion of raider",
			user.getName(), userId);
		logger.warning(guild, warningMessage);
	}

	private void startWatching (String userId, String guildId) {
		PotentialRaider toAdd = new PotentialRaider(userId, guildId, 1, System.currentTimeMillis());
		potentialRaiders.add(toAdd);

		new Timer().schedule(
			new TimerTask() {
				@Override
				public void run () {
					potentialRaiders.remove(toAdd);
				}
			}, SpamTolerance.MESSAGE.getDuration());
	}

	private boolean underWatch (String userId, String guildId) {
		return potentialRaiders
			.stream()
			.anyMatch(potentialRaider -> potentialRaider.getGuildId().equals(guildId)
				&& potentialRaider.getUserId().equals(userId));
	}

	private PotentialRaider getPotentialRaider (String userId, String guildId) {
		for (PotentialRaider r : potentialRaiders) {
			boolean match = r.getGuildId().equals(guildId) && r.getUserId().equals(userId);
			if (match) {
				return r;
			}
		}

		return null;
	}
}
