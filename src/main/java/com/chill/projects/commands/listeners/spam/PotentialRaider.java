package com.chill.projects.commands.listeners.spam;

public class PotentialRaider extends Raider {

	private int messageCount;
	private long initialMessageTime;

	public PotentialRaider (String userId, String guildId,
							int messageCount, long initialMessageTime) {

		super(userId, guildId);
		this.messageCount = messageCount;
		this.initialMessageTime = initialMessageTime;
	}

	public void incMessageCount () {
		messageCount++;
	}

	public int getMessageCount () {
		return messageCount;
	}

	public long getInitialMessageTime () {
		return initialMessageTime;
	}

	@Override
	public String getGuildId () {
		return super.getGuildId();
	}

	@Override
	public String getUserId () {
		return super.getUserId();
	}
}
