package com.chill.projects.commands.listeners;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentType;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.logging.Logging;
import com.chill.projects.macros.MacrosList;
import com.chill.projects.roles.PassionRole;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Arrays;
import java.util.List;

public class CommandListener extends ListenerAdapter {

	private Logging logger;
	private final String PLACEHOLDER = "*";

	public CommandListener (Logging logger) {
		this.logger = logger;
		CommandContainer.load();
	}

	@Override
	public void onGuildMessageReceived (GuildMessageReceivedEvent event) {

		if (event.getAuthor().isBot()) return;

		String message = event.getMessage().getContentRaw();
		if (!message.startsWith(PassionGuildList.find(event.getGuild().getId()).getPrefix())) {
			return;
		}

		Guild current = event.getGuild();
		User author = event.getAuthor();
		MessageChannel from = event.getChannel();

		String commandUsed = retrieveCommandUsed(message);

		if (MacrosList.find(commandUsed, current.getId()) != null) {
			return;
		}

		Command c;
		if ((c = CommandContainer.find(commandUsed)) == null) {
			from.sendMessage("Unable to find command **" + commandUsed + "**").queue();
			return;
		}

		logUse(commandUsed, current, author, from);

		if (!canInvokeCommand(c, current, author)) {
			from.sendMessage("Did you think I would let you do that :thinking:").queue();
			return;
		}

		String[] arguments = extractArguments(message);

		if (c.getArguments().length == 0) {
			c.invoke(current, author, from);
			return;
		}

		if (!hasSufficientArguments(c, arguments)) {
			from.sendMessage("You have an insufficient amount of arguments.").queue();
			return;
		}

		String[] merged = mergeSentences(c, arguments);
		if (hasInvalidOptionalArguments(c, merged)) {
			from.sendMessage("Invalid optional arguments placed").queue();
			return;
		}

		String[] commandArgs = generateCommandArguments(c, merged);
		if (!c.hasValidArguments(commandArgs, current, from)) {
			return;
		}

		c.invoke(current, author, from, commandArgs);
	}

	private String[] generateCommandArguments (Command c, String[] currentArgs) {
		Argument[] args = c.getArguments();
		String[] results = new String[args.length];

		for (int i = 0; i < args.length; i++) {
			results[i] = currentArgs[i];

			if (currentArgs[i].equals(PLACEHOLDER)) {
				results[i] = c.getArguments()[i].getDef();
			}
		}
		return results;
	}

	private boolean hasSufficientArguments (Command c, String[] arguments) {
		boolean hasSentence = Arrays
			.stream(c.getArguments())
			.anyMatch(arg -> arg.getType() == ArgumentType.SENTENCE);

		return (hasSentence && arguments.length >= c.getArguments().length) || (c.getArguments().length == arguments.length);
	}

	private String[] mergeSentences (Command c, String[] original) {
		Argument[] args = c.getArguments();
		String[] newArgs = new String[args.length];

		for (int i = 0; i < args.length; i++) {
			newArgs[i] = original[i];

			if (args[i].getType() == ArgumentType.SENTENCE) {
				String sentence = String.join(" ", Arrays.copyOfRange(original, i, original.length));
				newArgs[i] = sentence;
			}
		}

		return newArgs;
	}

	private boolean canInvokeCommand (Command c, Guild g, User invoker) {
		List<Role> invokerRoles = g.getMember(invoker).getRoles();

		if (g.getMember(invoker).isOwner()) return true;

		if (c.getMinimum() == PassionRole.MEMBER) return true;

		if (invokerRoles.size() == 0) return false;

		Role minimum = g.getRolesByName(c.getMinimum().getName(), true).get(0);

		int invokerPosition = invokerRoles.get(0).getPosition();
		int minimumPosition = minimum.getPosition();

		return invokerPosition >= minimumPosition;
	}

	private boolean hasInvalidOptionalArguments (Command c, String[] args) {
		Argument[] temp = c.getArguments();

		for (int i = 0; i < args.length; i++) {
			if (!temp[i].isOptional() && args[i].equals(PLACEHOLDER)) {
				return true;
			}
		}
		return false;
	}

	private String retrieveCommandUsed (String message) {
		message += " ";
		return message.substring(1, message.indexOf(" "));
	}

	private String[] extractArguments (String message) {
		message += " ";
		String[] extracted = message.split(" ");
		return Arrays.copyOfRange(extracted, 1, extracted.length);
	}

	private void logUse (String commandName, Guild guild, User invoker, MessageChannel from) {
		logger.normal(guild, "Command: **" + commandName
			+ "** invoked by **" + invoker.getName() +
			"#" + invoker.getDiscriminator() + "** :: ID :: **" + invoker.getId()
			+ "** in **" + from.getName() + "**");
	}
}
