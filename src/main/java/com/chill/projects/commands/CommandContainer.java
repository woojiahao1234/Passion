package com.chill.projects.commands;

import com.chill.projects.commands.types.*;

import java.util.ArrayList;
import java.util.List;

public class CommandContainer {

	private static List<Command> commands = new ArrayList<>();

	public static void add (Command c) {
		commands.add(c);
	}

	public static Command find (String name) {
		for (Command  c : commands) {
			if (c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

	public static void load () {
		new UtilityCommands();
		new ModerationCommands();
		new AdministrationCommands();
		new LoggingCommands();
		new FunCommands();
		new MacroCommands();
		new InfractionCommands();
	}
}
