package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentType;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.roles.RoleManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.requests.RestAction;

import java.awt.*;
import java.sql.SQLException;

import static com.chill.projects.commands.arguments.ArgumentType.MESSAGE_ID;
import static com.chill.projects.commands.arguments.ArgumentType.SENTENCE;
import static com.chill.projects.commands.arguments.ArgumentType.WORD;
import static com.chill.projects.roles.PassionRole.ADMINISTRATOR;
import static com.chill.projects.roles.PassionRole.OWNER;

public class AdministrationCommands {

	public AdministrationCommands () {
		CommandContainer.add(
			new Command("roles", OWNER) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					RoleManager manager = new RoleManager();
					manager.loadRoles(guild);
					manager.autoAssignRoles(guild);
				}
			}
		);

		CommandContainer.add(
			new Command("setprefix", OWNER,
				new Argument.Builder().setType(ArgumentType.WORD).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String targetPrefix = args[0];

					if (targetPrefix.length() > 1) {
						from.sendMessage("Keep the prefix to only one character.").queue();
						return;
					}

					if (targetPrefix.equals("*")) {
						from.sendMessage("Prefix cannot be the same as the placeholder").queue();
					}

					try {
						PassionGuildList.find(guild.getId()).setPrefix(targetPrefix);
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Unable to set prefix").queue();
					}

					from.sendMessage("New prefix is **" + targetPrefix + "**.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("resetprefix", OWNER) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					try {
						PassionGuildList.find(guild.getId()).setPrefix("?");
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Unable to reset prefix").queue();
					}

					from.sendMessage("Prefix has been reset to **?**.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("respond", ADMINISTRATOR,
				new Argument.Builder().setType(MESSAGE_ID).build(),
				new Argument.Builder().setType(WORD).setToOptional("Accepted").setRestrictedArgs("Accepted", "Declined").build(),
				new Argument.Builder().setType(SENTENCE).setToOptional("Good Idea").build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String suggestionId = args[0];
					String status = args[1];
					String reason = args[2];

					String suggestionChannelId = PassionGuildList.find(guild.getId()).getSuggestionId();
					TextChannel suggestionChannel = guild.getTextChannelById(suggestionChannelId);
					RestAction<Message> ra = null;

					try {
						ra = suggestionChannel.getMessageById(suggestionId);
					} catch (Exception e) {
						from.sendMessage("Unable to find suggestion of id: **" + suggestionId + "**").queue();
						return;
					}

					MessageEmbed original = ra.complete().getEmbeds().get(0);

					Color statusColor = null;
					if (status.equalsIgnoreCase("Accepted")) statusColor = new Color(96, 255, 68);
					else if (status.equalsIgnoreCase("Declined")) statusColor = new Color(255, 68, 68);
					else statusColor = new Color(0, 203, 255);

					MessageEmbed.Field suggestionField = original.getFields().get(0);
					MessageEmbed edited = new EmbedBuilder()
						.addField(suggestionField)
						.addField("Suggestion Status", status, false)
						.addField("Reason", reason, false)
						.setThumbnail(original.getThumbnail().getUrl())
						.setColor(statusColor)
						.build();

					ra.complete().editMessage(edited).queue();
				}
			}
 		);
	}
}
