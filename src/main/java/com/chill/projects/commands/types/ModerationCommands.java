package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentType;
import com.chill.projects.commands.listeners.spam.RaidersList;
import com.chill.projects.directmessage.PassionDirectMessage;
import com.chill.projects.roles.PassionRole;
import com.chill.projects.roles.RoleManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;

import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.chill.projects.commands.arguments.ArgumentType.MEMBER_ID;
import static com.chill.projects.roles.PassionRole.MODERATOR;

public class ModerationCommands {

	private PassionDirectMessage directMessage;

	public ModerationCommands () {
		directMessage = new PassionDirectMessage();

		CommandContainer.add(
			new Command("mute", MODERATOR, muteArguments()) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {

					int duration = Integer.parseInt(args[0]);
					String targetId = args[1];
					String reason = args[2];

					RoleManager manager = new RoleManager();
					Member target = guild.getMemberById(targetId);
					manager.giveRole(target, guild, PassionRole.MUTED);

					from.sendMessage(target.getEffectiveName() + " has been muted for " + duration + " minute(s).").queue();
					directMessage.sendDirectMessage(
						new EmbedBuilder()
							.addField("Muted", "You have been muted", false)
							.addField("Reason", reason, true)
							.addField("Duration (in minutes)", String.valueOf(duration), true)
							.setColor(new Color(255, 53, 53))
							.build(),
						target.getUser()
					);

					new Timer().schedule(new TimerTask() {
						@Override
						public void run () {
							manager.removeRole(target, guild, PassionRole.MUTED);
							from.sendMessage(target.getEffectiveName() + " has been unmuted.").queue();
						}
					}, duration * 60000);
				}
			}
		);

		CommandContainer.add(
			new Command("nuke", MODERATOR,
				new Argument.Builder().setType(ArgumentType.INT).setToOptional("5").setRange(1, 99).build()) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {

					int numberToNuke = Integer.parseInt(args[0]);

					TextChannel target = guild.getTextChannelById(from.getId());
					List<Message> messages = new MessageHistory(from)
						.retrievePast(numberToNuke + 1)
						.complete();
					target.deleteMessages(messages)
						.queue();
				}
			}
		);

		CommandContainer.add(
			new Command("grant", MODERATOR, rolesArguments()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String roleToGive = args[0];
					String targetId = args[1];

					RoleManager manager = new RoleManager();
					Member target = guild.getMemberById(targetId);
					manager.giveRole(target, guild, PassionRole.valueOf(roleToGive.toUpperCase()));

					from.sendMessage("User: **" + target.getEffectiveName()
						+ "** has been granted **" + roleToGive + "**.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("revoke", MODERATOR, rolesArguments()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String roleToGive = args[0];
					String targetId = args[1];

					RoleManager manager = new RoleManager();
					Member target = guild.getMemberById(targetId);
					manager.removeRole(target, guild, PassionRole.valueOf(roleToGive.toUpperCase()));

					from.sendMessage("User: **" + target.getEffectiveName()
						+ "** has been revoked of the **" + roleToGive + "** role.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("freeraider", MODERATOR,
				new Argument.Builder().setType(MEMBER_ID).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					RoleManager manager = new RoleManager();
					String targetToFree = args[0];

					Member toFree = guild.getMemberById(targetToFree);
					manager.removeRole(toFree, guild, PassionRole.MUTED);

					try {
						RaidersList.remove(toFree.getUser().getId(), guild.getId());
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Unable to free raider").queue();
					}

					from.sendMessage(toFree.getEffectiveName() + " has been freed").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("viewraiders", MODERATOR) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					StringBuilder toPrint = new StringBuilder("Raiders in **" + guild.getName() + "**:\n");

					RaidersList.find(guild.getId())
						.forEach(raider -> toPrint.append(raider.getUserId()).append(" "));

					from.sendMessage(toPrint.toString()).queue();
				}
			}
		);

		CommandContainer.add(
			new Command("viewcreationdate", MODERATOR,
				new Argument.Builder().setType(MEMBER_ID).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String targetId = args[0];
					Member target = guild.getMemberById(targetId);

					String creationDate = target.getUser().getCreationTime().toString();

					String toSend = String.format("Creation date of **%s#%s**: %s",
						target.getEffectiveName(),
						target.getUser().getDiscriminator(),
						creationDate);
					from.sendMessage(toSend).queue();
				}
			}
		);
	}

	private Argument[] muteArguments () {
		List<Argument> args = new ArrayList<>();

		args.add(new Argument.Builder().setType(MEMBER_ID)
			.build());
		args.add(new Argument.Builder()
			.setType(ArgumentType.INT)
			.setToOptional("15")
			.build());
		args.add(new Argument.Builder()
			.setType(ArgumentType.SENTENCE)
			.setToOptional("Shut it")
			.build());

		return args.toArray(new Argument[args.size()]);
	}

	private Argument[] rolesArguments () {
		List<Argument> args = new ArrayList<>();

		args.add(new Argument.Builder().setType(ArgumentType.WORD).build());
		args.add(new Argument.Builder().setType(MEMBER_ID).build());

		return args.toArray(new Argument[args.size()]);
	}
}
