package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.database.IQueryData;
import com.chill.projects.database.QueryData;
import com.chill.projects.directmessage.PassionDirectMessage;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.chill.projects.commands.arguments.ArgumentType.*;
import static com.chill.projects.roles.PassionRole.MODERATOR;

public class InfractionCommands {

	public InfractionCommands () {
		CommandContainer.add(
			new Command("warn", MODERATOR,
				new Argument.Builder().setType(MEMBER_ID).build(),
				new Argument.Builder().setType(SENTENCE).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String targetId = args[0];
					String reason = args[1];

					infract(0, guild.getMemberById(targetId), sender, guild, reason);
				}
			}
		);

		CommandContainer.add(
			new Command("strike", MODERATOR,
				new Argument.Builder().setType(MEMBER_ID).build(),
				new Argument.Builder().setType(INT).setToOptional("1").setRange(1, 3).build(),
				new Argument.Builder().setType(SENTENCE).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String targetId = args[0];
					int weight = Integer.parseInt(args[1]);
					String reason = args[2];

					infract(weight, guild.getMemberById(targetId), sender, guild, reason);
				}
			}
		);
	}

	private void infract (int weight, Member target, User actingMod, Guild from, String reason) {
		String userId = target.getUser().getId();
		String guildId = from.getId();

		updateWeightTable(weight, userId, guildId);
		updateReasonTable(reason, userId, guildId);

		infractionAction(reason, target.getUser(), from);

		sendInfractToTarget(weight, reason, target.getUser());
		sendInfractToActingMod(weight, reason, actingMod, target.getUser());
	}

	private void sendInfractToActingMod (int weight, String reason, User actingMod, User target) {
		PassionDirectMessage directMessage = new PassionDirectMessage();

		EmbedBuilder infractionEmbed = new EmbedBuilder();

		infractionEmbed.setColor(new Color(228, 76, 255));
		infractionEmbed.addField("Infraction made", "You have made an infraction on " + target.getName(), false);
		infractionEmbed.addField("Reason", reason, true);
		infractionEmbed.addField("Weight", String.valueOf(weight), true);

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		infractionEmbed.setFooter("Infraction made on " + dateFormat.format(date),
			actingMod.getAvatarUrl());

		directMessage.sendDirectMessage(infractionEmbed.build(), actingMod);
	}

	private void sendInfractToTarget (int weight, String reason, User target) {
		PassionDirectMessage directMessage = new PassionDirectMessage();

		EmbedBuilder infractionEmbed = new EmbedBuilder();

		infractionEmbed.setColor(new Color(255, 74, 38));
		infractionEmbed.addField("Infraction", "You have been infracted due to bad behavior", false);
		infractionEmbed.addField("Reason", reason, true);
		infractionEmbed.addField("Weight", String.valueOf(weight), true);

		directMessage.sendDirectMessage(infractionEmbed.build(), target);
	}

	private void updateWeightTable (int weight, String userId, String guildId) {
		final String INFRACTION = "InfractionWeight";
		IQueryData queryData = new QueryData();

		String whereClause = String.format("user_id = '%s' AND guild_id = '%s'",
			userId, guildId);
		try {
			ResultSet recordExist = queryData.select("*", INFRACTION, whereClause);

			if (!recordExist.next()) {
				String values = String.format("'%s', '%s', %s",
					userId, guildId, weight);
				queryData.insert("user_id, guild_id, weight", INFRACTION, values);
			} else {
				int currentWeight = recordExist.getInt(3);
				int newWeight = currentWeight + weight;

				String set = String.format("weight = %s", newWeight);
				queryData.update(set, INFRACTION, whereClause);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Unable to update InfractionWeight");
		}

	}

	private void infractionAction (String reason, User target, Guild guild) {

		int weight = 0;

		try {
			ResultSet resultSet = new QueryData().select("weight",
				"InfractionWeight",
				"user_id = '" + target.getId() + "' AND guild_id = '" + guild.getId() + "'");

			while (resultSet.next()) {
				weight = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Unable to get weight");
		}

		if (weight >= 3) {
			guild.getController().ban(target, 0, reason).complete();
		}
	}

	private void updateReasonTable (String reason, String userId, String guildId) {
		final String REASON = "InfractionReason";
		IQueryData queryData = new QueryData();

		try {
			String vals = String.format("'%s', '%s', '%s'",
				userId, guildId, reason);
			queryData.insert("user_id, guild_id, reason", REASON, vals);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Unable to update InfractionReason");
		}
	}
}
