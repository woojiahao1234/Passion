package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentType;
import com.chill.projects.macros.Macro;
import com.chill.projects.macros.MacrosList;
import com.chill.projects.roles.PassionRole;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MacroCommands {

	public MacroCommands () {
		CommandContainer.add(
			new Command("addmacro", PassionRole.MODERATOR, macroArguments()) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String macroName = args[0];
					String guildId = guild.getId();
					String macroText = args[1];

					if (CommandContainer.find(macroName) != null) {
						from.sendMessage("Unable to add macro **" + macroName + "** as there is a command after that").queue();
						return;
					}

					if (MacrosList.find(macroName, guildId) != null) {
						from.sendMessage("Unable to add macro **" + macroName + "** as it already exists").queue();
						return;
					}

					MacrosList.add(new Macro(macroName, guildId, macroText));
					from.sendMessage("**" + macroName + "** will now respond with **" + macroText + "**.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("removemacro", PassionRole.MODERATOR,
				new Argument.Builder().setType(ArgumentType.WORD).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String macroName = args[0];
					String guildId = guild.getId();

					if (CommandContainer.find(macroName) != null && MacrosList.find(macroName, guildId) == null) {
						from.sendMessage("Unable to remove macro **" + macroName + "** as it does not exist.").queue();
						return;
					}

					MacrosList.remove(macroName, guildId);
					from.sendMessage("**" + macroName + "** has been removed.").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("viewmacros", PassionRole.MODERATOR) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					StringBuilder result = new StringBuilder("Macros in **" + guild.getName() + "**:\n");

					List<Macro> macros = MacrosList.getMacros()
						.stream()
						.filter(macro -> macro.getGuildId().equals(guild.getId()))
						.collect(Collectors.toList());

					String[] names = new String[macros.size()];
					for (int i = 0; i < macros.size(); i++) {
						names[i] = macros.get(i).getName();
					}

					result.append(String.join(", ", names));
					from.sendMessage(result.toString()).queue();
				}
			}
		);
	}

	private Argument[] macroArguments () {
		List<Argument> args = new ArrayList<>();

		args.add(new Argument.Builder().setType(ArgumentType.WORD).build());
		args.add(new Argument.Builder().setType(ArgumentType.SENTENCE).build());

		return args.toArray(new Argument[args.size()]);
	}
}
