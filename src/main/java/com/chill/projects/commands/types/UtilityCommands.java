package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.directmessage.PassionDirectMessage;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.help.Help;
import com.chill.projects.help.HelpList;
import net.dv8tion.jda.bot.entities.ApplicationInfo;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.requests.RestAction;

import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.chill.projects.commands.arguments.ArgumentType.SENTENCE;
import static com.chill.projects.commands.arguments.ArgumentType.WORD;
import static net.dv8tion.jda.core.OnlineStatus.OFFLINE;

public class UtilityCommands {

	public UtilityCommands () {
		CommandContainer.add(
			new Command("ping") {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					from.sendMessage("Pong!").queue();
				}
			}
		);

		CommandContainer.add(
			new Command("google",
				new Argument.Builder().setType(SENTENCE).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {

					String searchTerm = args[0];
					String googleSearch = "http://www.google.com/search?q=" + searchTerm.replaceAll(" ", "+");

					from.sendMessage(googleSearch).queue();
				}
			}
		);

		CommandContainer.add(
			new Command("suggest",
				new Argument.Builder().setType(SENTENCE).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String suggestion = args[0];

					EmbedBuilder suggestionEmbed = new EmbedBuilder();
					suggestionEmbed.setThumbnail(sender.getAvatarUrl());
					suggestionEmbed.setColor(new Color(249, 227, 24));

					suggestionEmbed.addField(sender.getName() + " Suggestion", suggestion, false);
					suggestionEmbed.addField("Suggestion Status", "Under review", false);

					String suggestionChannelId = PassionGuildList.find(guild.getId()).getSuggestionId();
					if (suggestionChannelId == null) return;
					RestAction<Message> ra = guild
						.getTextChannelById(suggestionChannelId)
						.sendMessage(suggestionEmbed.build());
					Message m = ra.complete();

					m.addReaction("⬆").complete();
					m.addReaction("⬇").complete();
				}
			}
		);

		CommandContainer.add(
			new Command("serverinfo") {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					EmbedBuilder serverEmbed = new EmbedBuilder();

					ApplicationInfo info = guild.getJDA().asBot().getApplicationInfo().complete();
					String guildCreationDate = guild.getCreationTime().toString();
					String guildName = guild.getName();

					long onlineUsers = guild
						.getMembers()
						.stream()
						.filter(member -> member.getOnlineStatus() != OFFLINE)
						.count();
					String users = String.format("%s/%s", onlineUsers, guild.getMembers().size());

					serverEmbed.setColor(new Color(66, 255, 122));
					serverEmbed.setFooter(guildCreationDate, info.getIconUrl());
					serverEmbed.setThumbnail(guild.getIconUrl());

					serverEmbed.addField(guildName, "Fun server to goof off in", false);
					serverEmbed.addField("Users", users, true);
					serverEmbed.addField("Total Roles", String.valueOf(guild.getRoles().size()), true);
					serverEmbed.addField("Owner", guild.getOwner().getEffectiveName(), true);
					serverEmbed.addField("Region", guild.getRegion().toString(), true);
					serverEmbed.addField("Text Channels", String.valueOf(guild.getTextChannels().size()), true);
					serverEmbed.addField("Voice Channels", String.valueOf(guild.getVoiceChannels().size()), true);

					from.sendMessage(serverEmbed.build()).queue();
				}
			}
		);

		CommandContainer.add(
			new Command("help",
				new Argument.Builder().setType(WORD).setToOptional("me").build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String toHelp = args[0];

					PassionDirectMessage dm = new PassionDirectMessage();
					EmbedBuilder helpEmbed = new EmbedBuilder();
					helpEmbed.setColor(new Color(22, 255, 251));

					Member member = guild.getMember(sender);

					ApplicationInfo info = guild.getJDA().asBot().getApplicationInfo().complete();

					Help h;

					if (toHelp.equalsIgnoreCase("me")) {
						helpEmbed.setFooter("Bot made by Chill", guild.getOwner().getUser().getAvatarUrl());
						helpEmbed.setThumbnail(info.getIconUrl());

						helpEmbed.addField("Passion help menu",
							"Use `?help <command/category>` to receive help for a particular command",
							false);
						helpEmbed.addField("Currently available categories",
							String.join(", ", HelpList.getCategories()),
							false);
					} else {
						h = HelpList.findHelp(toHelp, member, guild);

						if (h == null) {
							List<String> category;

							if ((category = HelpList.findCategory(toHelp, member, guild)).size() == 0) {
								helpEmbed.addField("Unable to find command",
									"I was unable to find the command/category with name: **" + toHelp + "**",
									false);
							} else {
								String availableCommands = String.join(", ", category);
								helpEmbed.addField("Commands under " + toHelp + " category",
									availableCommands,
									false);
							}
						} else {
							String description = h.getDescription();
							String syntax = h.getSyntax();
							String example = h.getExample();

							helpEmbed.addField(h.getCategory() + " - " + h.getName(), description, false);
							helpEmbed.addField("Syntax", syntax, true);
							helpEmbed.addField("Example", example, true);
						}

						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date date = new Date();

						helpEmbed.setFooter("Help invoked on " + dateFormat.format(date),
							sender.getAvatarUrl());
					}

					dm.sendDirectMessage(helpEmbed.build(), sender);
				}
			}
		);

		CommandContainer.add(
			new Command("listavailable") {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					EmbedBuilder helpEmbed = new EmbedBuilder();
					Member invoker = guild.getMember(sender);

					helpEmbed.setThumbnail(sender.getAvatarUrl());
					helpEmbed.setTitle("Commands available to you");
					helpEmbed.setColor(new Color(22, 255, 251));

					for (String category : HelpList.getCategories()) {
						List<String> availableCommands = HelpList.findCategory(category, invoker, guild);
						if (availableCommands.size() == 0) continue;
						helpEmbed.addField(category, String.join(", ", availableCommands), false);
					}

					from.sendMessage(helpEmbed.build()).queue();
				}
			}
		);
	}
}
