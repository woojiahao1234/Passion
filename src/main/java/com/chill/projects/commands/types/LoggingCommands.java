package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.database.QueryData;
import com.chill.projects.guilds.PassionGuild;
import com.chill.projects.guilds.PassionGuildList;
import com.chill.projects.roles.PassionRole;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoggingCommands {

	public LoggingCommands () {

		CommandContainer.add(
			new Command("setlog", PassionRole.OWNER) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					PassionGuild target = PassionGuildList.find(guild.getId());

					if (target == null) {
						return;
					}

					from.sendMessage("Current channel set as logging channel").queue();
					try {
						target.setCommandId(from.getId());
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Error occurred when trying to set logging channel").queue();
					}
				}
			}
		);

		CommandContainer.add(
			new Command("setgeneral", PassionRole.OWNER) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					PassionGuild target = PassionGuildList.find(guild.getId());

					if (target == null) {
						return;
					}

					from.sendMessage("Current channel set as general channel").queue();
					try {
						target.setGeneralId(from.getId());
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Error occurred when trying to set general channel").queue();
					}
				}
			}
		);

		CommandContainer.add(
			new Command("setwarning", PassionRole.OWNER) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					PassionGuild target = PassionGuildList.find(guild.getId());

					if (target == null) {
						return;
					}

					from.sendMessage("Current channel set as warning channel").queue();
					try {
						target.setWarningId(from.getId());
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Error occurred when trying to set warning channel").queue();
					}
				}
			}
		);

		CommandContainer.add(
			new Command("setsuggestion", PassionRole.OWNER) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					PassionGuild target = PassionGuildList.find(guild.getId());

					if (target == null) {
						return;
					}

					from.sendMessage("Current channel set as suggestion channel").queue();
					try {
						target.setSuggestionId(from.getId());
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Error occurred when trying to set suggestion channel").queue();
					}
				}
			}
		);

		CommandContainer.add(
			new Command("viewlogging", PassionRole.SRMODERATOR) {
				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					try {
						ResultSet results = new QueryData()
							.select("*", "PassionGuild", "guild_id = '" + guild.getId() + "'");

						while (results.next()) {
							String loggingId = results.getString(2);
							String generalId = results.getString(3);
							String suggestionId = results.getString(4);
							String warningId = results.getString(5);

							StringBuilder toPrint = new StringBuilder("Logging channels for **" + guild.getName() + "**:\n");
							toPrint.append("**General** ==> **").append(generalId).append("**\n");
							toPrint.append("**Logging** ==> **").append(loggingId).append("**\n");
							toPrint.append("**Suggestion** ==> **").append(suggestionId).append("**\n");
							toPrint.append("**Warning** ==> **").append(warningId).append("**\n");

							from.sendMessage(toPrint.toString()).queue();
						}
					} catch (SQLException e) {
						e.printStackTrace();
						from.sendMessage("Unable to view logging channels").queue();
					}
				}
			}
		);
	}
}
