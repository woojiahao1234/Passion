package com.chill.projects.commands.types;

import com.chill.projects.commands.Command;
import com.chill.projects.commands.CommandContainer;
import com.chill.projects.commands.arguments.Argument;
import com.chill.projects.commands.arguments.ArgumentType;
import com.chill.projects.roles.PassionRole;
import net.dv8tion.jda.bot.entities.ApplicationInfo;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.awt.*;

import static net.dv8tion.jda.core.OnlineStatus.OFFLINE;

public class FunCommands {

	public FunCommands () {
		CommandContainer.add(
			new Command("echo", PassionRole.MODERATOR,
				new Argument.Builder().setType(ArgumentType.CHANNEL_ID).build(),
				new Argument.Builder().setType(ArgumentType.SENTENCE).build()) {

				@Override
				public void invoke (Guild guild, User sender, MessageChannel from, String... args) {
					String targetChannelId = args[0];
					String message = args[1];

					MessageChannel targetChannel = guild.getTextChannelById(targetChannelId);
					targetChannel.sendMessage(message).queue();
				}
			}
		);
	}
}
