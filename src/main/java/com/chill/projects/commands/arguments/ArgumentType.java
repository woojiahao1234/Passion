package com.chill.projects.commands.arguments;

public enum ArgumentType {
	INT,
	DOUBLE,
	WORD,
	SENTENCE,
	MEMBER_ID,
	CHANNEL_ID,
	MESSAGE_ID	;
}
