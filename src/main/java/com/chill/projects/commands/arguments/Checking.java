package com.chill.projects.commands.arguments;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;

public interface Checking {
	boolean check (String arg, Guild guild, MessageChannel from, Argument argument);
}
