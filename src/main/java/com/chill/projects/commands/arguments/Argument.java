package com.chill.projects.commands.arguments;

public class Argument {

	private boolean isOptional;
	private ArgumentType type;
	private String def;
	private String[] restrictedArgs;
	private int[] range;

	public static class Builder {
		private boolean isOptional;
		private ArgumentType type;
		private String def;
		private String[] restrictedArgs;
		private int[] range;

		public Builder () {
			isOptional = false;
			type = ArgumentType.WORD;
			def = "";
			restrictedArgs = null;
			range = null;
		}

		public Builder setType (ArgumentType type) {
			this.type = type;
			return this;
		}

		public Builder setToOptional (String defaultValue) {
			isOptional = true;
			this.def = defaultValue;
			return this;
		}

		public Builder setRestrictedArgs (String... args) {
			this.restrictedArgs = args;
			return this;
		}

		public Builder setRange (int lower, int upper) {
			this.range = new int[] { lower, upper };
			return this;
		}

		public Argument build () {

			if (type == ArgumentType.INT || type == ArgumentType.DOUBLE) {
				return new Argument(type, isOptional, def, range);
			} else {
				return new Argument(type, isOptional, def, restrictedArgs);
			}
		}
	}

	private Argument (ArgumentType type, boolean isOptional, String def, int[] range) {
		this.type = type;
		this.isOptional = isOptional;
		this.def = def;
		this.range = range;
	}

	private Argument (ArgumentType type, boolean isOptional, String def, String[] restrictedArgs) {
		this.type = type;
		this.isOptional = isOptional;
		this.def = def;
		this.restrictedArgs = restrictedArgs;
	}

	public boolean isOptional () {
		return isOptional;
	}

	public ArgumentType getType () {
		return type;
	}

	public String getDef () {
		return def;
	}

	public String[] getRestrictedArgs () {
		return restrictedArgs;
	}

	public int getLowerRange () {
		return range[0];
	}

	public int getUpperRange () {
		return range[1];
	}
}
