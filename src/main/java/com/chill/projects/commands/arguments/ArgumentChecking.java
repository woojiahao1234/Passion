package com.chill.projects.commands.arguments;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.HashMap;
import java.util.Map;

import static com.chill.projects.commands.arguments.ArgumentType.*;

public class ArgumentChecking {
	private static Map<ArgumentType, Checking> checks = loadChecks();

	private static Map<ArgumentType, Checking> loadChecks () {
		Map<ArgumentType, Checking> result = new HashMap<>();

		result.put(INT, ArgumentChecking::intCheck);
		result.put(DOUBLE, ArgumentChecking::doubleCheck);
		result.put(WORD, ArgumentChecking::wordCheck);
		result.put(MEMBER_ID, ArgumentChecking::memberIdCheck);
		result.put(CHANNEL_ID, ArgumentChecking::channelIdCheck);
		result.put(SENTENCE, ArgumentChecking::sentenceCheck);
		result.put(MESSAGE_ID, ArgumentChecking::messageIdCheck);

		return result;
	}

	public static Map<ArgumentType, Checking> getChecks () {
		return checks;
	}

	private static boolean sentenceCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		return true;
	}

	private static boolean channelIdCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		if (guild.getTextChannelById(arg) == null) {
			from.sendMessage("Invalid channel id: **" + arg + "** entered").queue();
			return false;
		}

		return true;
	}

	private static boolean memberIdCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		if (guild.getMemberById(arg) == null) {
			from.sendMessage("Invalid user id: **" + arg + "** entered").queue();
			return false;
		}

		return true;
	}

	private static boolean doubleCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		try {
			double num = Double.parseDouble(arg);

			if (num < argument.getLowerRange() || num > argument.getUpperRange()) {
				from.sendMessage("Invalid number entered. " +
					"Please enter a number between " + argument.getLowerRange() + " - " + argument.getUpperRange())
					.queue();
				return false;
			}

			return true;
		} catch (NumberFormatException e) {
			from.sendMessage("Invalid double used as argument").queue();
			return false;
		}
	}

	private static boolean intCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		try {
			int num = Integer.parseInt(arg);
			if (num < argument.getLowerRange() || num > argument.getUpperRange()) {
				from.sendMessage("Invalid number entered. " +
					"Please enter a number between " + argument.getLowerRange() + " - " + argument.getUpperRange())
					.queue();
				return false;
			}

			return true;
		} catch (NumberFormatException e) {
			from.sendMessage("Invalid int used as argument").queue();
			return false;
		}
	}

	private static boolean wordCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		String[] restrictedArgs = argument.getRestrictedArgs();
		if (restrictedArgs == null) return true;

		for (String restrictedArg : restrictedArgs) {
			if (arg.toLowerCase().equals(restrictedArg.toLowerCase())) return true;
		}

		from.sendMessage("Invalid argument entered. You must enter the following: " + String.join(", ", restrictedArgs)).queue();

		return false;
	}

	private static boolean messageIdCheck (String arg, Guild guild, MessageChannel from, Argument argument) {
		return guild.getTextChannels()
			.stream()
			.anyMatch(textChannel -> textChannel.getMessageById(arg) != null);
	}
}
