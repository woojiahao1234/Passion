package com.chill.projects.commands;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public interface ICommand {
	void invoke (Guild guild, User sender, MessageChannel from, String... args);
}
