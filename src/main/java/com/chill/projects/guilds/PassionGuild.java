package com.chill.projects.guilds;

import com.chill.projects.database.IQueryData;
import com.chill.projects.database.QueryData;

import java.sql.SQLException;

public class PassionGuild {

	private String guildId;
	private String commandId;
	private String suggestionId;
	private String generalId;
	private String warningId;

	private String prefix;

	private IQueryData queryData;

	public PassionGuild (String guildId, String commandId,
						 String suggestionId, String generalId,
						 String warningId, String prefix) {

		this.guildId = guildId;
		this.commandId = commandId;
		this.suggestionId = suggestionId;
		this.generalId = generalId;
		this.warningId = warningId;
		this.prefix = prefix;

		queryData = new QueryData();
	}

	public String getGuildId () {
		return guildId;
	}

	public String getCommandId () {
		return commandId;
	}

	public String getSuggestionId () {
		return suggestionId;
	}

	public String getGeneralId () {
		return generalId;
	}

	public String getWarningId () {
		return warningId;
	}

	public void setCommandId (String commandId) throws SQLException {
		this.commandId = commandId;

		queryData.update("guild_command = '" + commandId + "'", "PassionGuild", "guild_id = '" + guildId + "'");
	}

	public void setGeneralId (String generalId) throws SQLException {
		this.generalId = generalId;

		queryData.update("guild_general = '" + generalId + "'", "PassionGuild", "guild_id = '" + guildId + "'");
	}

	public void setWarningId (String warningId) throws SQLException {
		this.warningId = warningId;

		queryData.update("guild_warning = '" + warningId + "'", "PassionGuild", "guild_id = '" + guildId + "'");
	}

	public void setSuggestionId(String suggestionId) throws SQLException {
		this.suggestionId = suggestionId;

		queryData.update("guild_suggestion = '" + suggestionId + "'", "PassionGuild", "guild_id = '" + guildId + "'");
	}

	public String getPrefix () {
		return prefix;
	}

	public void setPrefix (String prefix) throws SQLException {
		this.prefix = prefix;

		queryData.update("prefix = '" + prefix + "'", "Prefix", "guild_id = '" + guildId + "'");
	}
}
