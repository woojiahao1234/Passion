package com.chill.projects.guilds;

import com.chill.projects.database.IQueryData;
import com.chill.projects.database.QueryData;
import net.dv8tion.jda.core.entities.Guild;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PassionGuildList {

	private static List<PassionGuild> guilds = new ArrayList<>();
	private static IQueryData queryData = new QueryData();

	public static void load (List<Guild> g) throws SQLException {
		ResultSet result = queryData.select("g.*, p.prefix", "Prefix p, PassionGuild g", "p.guild_id = g.guild_id");

		while (result.next()) {
			String id = result.getString(1);
			String command = result.getString(2);
			String general = result.getString(3);
			String suggestion = result.getString(4);
			String warning = result.getString(5);
			String prefix = result.getString(6);

			PassionGuild guild = new PassionGuild(id, command, suggestion, general, warning, prefix);
			guilds.add(guild);
		}

		loadExistingGuilds(g);
	}

	public static PassionGuild find (String id) {
		for (PassionGuild g : guilds) {
			if (g.getGuildId().equals(id)) {
				return g;
			}
		}

		return null;
	}

	public static void add (PassionGuild guild) throws SQLException {
		guilds.add(guild);

		String vals = String.format("'%s', '%s', '%s', '%s', '%s'",
			guild.getGuildId(),
			guild.getCommandId(),
			guild.getGeneralId(),
			guild.getSuggestionId(),
			guild.getWarningId());

		queryData.insert("guild_id, guild_command, guild_general, guild_suggestion, guild_warning",
			"PassionGuild",
			vals);

		String prefixVals = String.format("'%s', '?'", guild.getGuildId());
		queryData.insert("guild_id, prefix", "Prefix", prefixVals);
	}

	public static void remove (String name) throws SQLException {

		PassionGuild guild = find(name);

		if (guild != null) {
			guilds.remove(guild);
		}

		queryData.delete("PassionGuild", "guild_id = '" + guild.getGuildId() + "'");
		queryData.delete("Prefix", "guild_id = '" + guild.getGuildId() + "'");
	}

	public static List<PassionGuild> getGuilds () {
		return guilds;
	}

	private static void loadExistingGuilds (List<Guild> g) {
		g.forEach(guild -> {
			if (find(guild.getId()) == null) {
				String defaultId = guild.getDefaultChannel().getId();
				PassionGuild toAdd = new PassionGuild(guild.getId(), defaultId, defaultId, defaultId, defaultId, "?");
				guilds.add(toAdd);
			}
		});
	}
}
