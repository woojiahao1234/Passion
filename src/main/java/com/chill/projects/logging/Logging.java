package com.chill.projects.logging;

import com.chill.projects.guilds.PassionGuild;
import com.chill.projects.guilds.PassionGuildList;
import net.dv8tion.jda.core.entities.Guild;

public class Logging {

	public void normal (Guild guild, String message) {
		PassionGuild target;
		if ((target = PassionGuildList.find(guild.getId())) == null) {
			 return;
		}

		String commandLoggingId = target.getCommandId();
		guild.getTextChannelById(commandLoggingId).sendMessage(message).queue();
	}

	public void warning (Guild guild, String message) {
		PassionGuild target;

		if ((target = PassionGuildList.find(guild.getId())) == null) {
			return;
		}

		String warningChannelId = target.getWarningId();
		guild.getTextChannelById(warningChannelId).sendMessage(message).queue();
	}
}
