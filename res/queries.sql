/*
Create the table for all of the guilds that will be hosting passion guild
 */
CREATE TABLE PassionGuild (
  guild_id TEXT NOT NULL,
  guild_command TEXT NULL,
  guild_general TEXT NULL,
  guild_suggestion TEXT NULL,
  guild_warning TEXT NULL,

  PRIMARY KEY (guild_id)
);

/*
Create the table for the macros system
 */
CREATE TABLE Macro (
  macro_name TEXT NOT NULL,
  guild_id TEXT NOT NULL,
  macro_text TEXT NOT NULL,

  PRIMARY KEY (macro_name, guild_id)
);

/*
Create the table for the prefix customization per guild
 */
CREATE TABLE Prefix (
  guild_id TEXT NOT NULL,
  prefix TEXT NOT NULL,

  PRIMARY KEY (guild_id)
);

/*
Create table for raiders
 */
CREATE TABLE Raider (
  user_id TEXT NOT NULL ,
  guild_id TEXT NOT NULL,

  PRIMARY KEY (user_id, guild_id)
);

/*
Creating a table for the infractions system
 */
CREATE TABLE InfractionWeight (
  user_id TEXT NOT NULL,
  guild_id TEXT NOT NULL,
  weight INTEGER NOT NULL,

  PRIMARY KEY (user_id, guild_id)
);

CREATE TABLE InfractionReason (
  reason_id SERIAL NOT NULL,
  user_id TEXT NOT NULL,
  guild_id TEXT NOT NULL,
  reason TEXT NOT NULL,

  PRIMARY KEY (reason_id)
);

